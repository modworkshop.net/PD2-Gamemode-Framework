Hooks:RegisterHook("GroupAIStatePostInit")

Hooks:PostHook(GroupAIManager, "init", "GamemodeGroupAIInitRegisterStates", function(self))
    self._registered_states = {
        ["empty"] = GroupAIStateEmpty,
        ["street"] = GroupAIStateStreet,
        ["besiege"] = GroupAIStateBesiege,
        ["airport"] = GroupAIStateBesiege,
        ["zombie_apocalypse"] = GroupAIStateBesiege
    }
    Hooks:Call("GroupAIStatePostInit", self)
end)

function GroupAIManager:register_state(name, object)
    if self._registered_states[name] then
        log(string.format("[ERROR] State with name %s already exists!", name))
    else
        self._registered_states[name] = object
    end
end

function GroupAIManager:set_state(name)
	if self._registered_states[name] then
		self._state = self._registered_states[name]:new()
	else
		Application:error("[GroupAIManager:set_state] inexistent state name", name)
		return
	end
	self._state_name = name
end
