core:import("CoreMenuItemOption")

local MenuCrimeNetContractInitiator_modify_node = MenuCrimeNetContractInitiator.modify_node

function MenuCrimeNetContractInitiator:modify_node(original_node, data)

    local node = MenuCrimeNetContractInitiator_modify_node(self, original_node, data)
    local item = node:item("job_gamemode")

    if item then
        local gamemodes = GamemodeFramework.managers.framework._loaded_mods
        local job_data = tweak_data.narrative:job_data(data.job_id)
        if job_data and job_data.allowed_gamemodes then
            gamemodes = job_data.allowed_gamemodes
        else
            item:add_option(CoreMenuItemOption.ItemOption:new({text_id = "menu_default", value = -1}))
        end

        local i = 1

        for k, v in pairs(gamemodes) do
            local gamemode = GamemodeFramework.managers.framework._loaded_mods[tonumber(k) ~= nil and v or k]
            local localize_title = managers.localization:exists(gamemode._config.id .. "TitleID")
            item:add_option(CoreMenuItemOption.ItemOption:new({text_id = localize_title and gamemode._config.id .. "TitleID" or gamemode._config.name, value = i, localize = localize_title, key = tonumber(k) ~= nil and v or k}))
            i = i + 1
        end

        item:set_current_index(1)
        item:_show_options(item._callback_handler)
        item:trigger()
    end

    return node
end

local LobbyOptionInitiator_modify_node = LobbyOptionInitiator.modify_node
function LobbyOptionInitiator:modify_node(node)
    node = LobbyOptionInitiator_modify_node(self, node)

    local item = node:item("job_gamemode")

    if item then
        item:clear_options()
        local gamemodes = GamemodeFramework.managers.framework._loaded_configs
        local job_data = tweak_data.narrative:job_data(managers.job:current_job_id())
        if job_data and job_data.allowed_gamemodes then
            gamemodes = job_data.allowed_gamemodes
        else
            item:add_option(CoreMenuItemOption.ItemOption:new({text_id = "menu_default", value = -1}))
        end

        local i = 1

        for k, v in pairs(gamemodes) do
            local gamemode = GamemodeFramework.managers.framework._loaded_configs[tonumber(k) ~= nil and v or k]
            local localize_title = managers.localization:exists(gamemode._config.id .. "TitleID")
            item:add_option(CoreMenuItemOption.ItemOption:new({text_id = localize_title and gamemode._config.id .. "TitleID" or gamemode._config.name, value = i, localize = localize_title, key = tonumber(k) ~= nil and v or k}))
            i = i + 1
        end

        if managers.job:current_job_id() then
            item:set_enabled(true)
        else
            item:set_enabled(false)
        end

        item:set_current_index(1)

        if Global.game_settings.gamemode then
            for i, option in pairs(item._all_options) do
                if option:parameters().key == Global.game_settings.gamemode then
                    item:set_current_index(i)
                end
            end
        end
        item:_show_options(item._callback_handler)
        item:trigger()
    end

    return node
end

function MenuCallbackHandler:choice_crimenet_lobby_gamemode(item)
    log(tostring(item:selected_option() and item:selected_option():parameters().key))
    GamemodeFramework.managers.framework:set_selected_gamemode(item:selected_option() and item:selected_option():parameters().key or nil)
end
