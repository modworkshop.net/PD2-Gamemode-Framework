if not GamemodeFramework then
    _G.GamemodeFramework = ModCore:new(ModPath .. "main_config.xml")
    local self = GamemodeFramework

    self.GamemodesPath = "Gamemodes"

    self.HooksDirectory = "Hooks/"
    self.ClassDirectory = "Classes/"

    self.Classes = {
        "GamemodeModCore.lua",
        "GamemodeFramework.lua",
        "GamemodeMenu.lua"
    }

    self.Hooks = {
        ["lib/tweak_data/tweakdata"] = "TweakData.lua",
        ["lib/managers/menumanager"] = "MenuManager.lua"
    }

    self.managers = {}
end

if RequiredScript then
    local requiredScript = RequiredScript:lower()
    if GamemodeFramework.Hooks[requiredScript] then
        dofile( GamemodeFramework.ModPath .. GamemodeFramework.HooksDirectory .. GamemodeFramework.Hooks[requiredScript] )
    end
end

--Need to use _init as init is used by ModuleBase class
function GamemodeFramework:_init()
    local success, ret = pcall(function()

    if not file.DirectoryExists(self.GamemodesPath) then
        os.execute("mkdir " .. self.GamemodesPath)
    end
    log("GamemodeFramework init")
    for _, class in pairs(self.Classes) do
        dofile(self.ModPath .. self.ClassDirectory .. class)
    end
    self:init_modules()
    self.managers.framework = GamemodeFrameworkManager:new()
    --self.managers.menu = GamemodeMenu:new()
end)
    if not success then
        log(tostring(ret))
    end
end

if not GamemodeFramework.setup then
    GamemodeFramework:_init()
    GamemodeFramework.setup = true
end

if Hooks then

end
