GamemodeFrameworkManager = GamemodeFrameworkManager or class(FrameworkBase)

GamemodeFrameworkManager._mod_core = GamemodeModCore

function GamemodeFrameworkManager:init()
    self._directory = GamemodeFramework.GamemodesPath
    self.super.init(self)
    if Global.load_level and Global.game_settings and Global.game_settings.gamemode and self._loaded_mods[Global.game_settings.gamemode] then
        self:LoadGamemodeHooks(self._loaded_mods[Global.game_settings.gamemode])
    end
end

function GamemodeFrameworkManager:set_selected_gamemode(key)
    Global.game_settings.gamemode = key

    --apply changes to lobby
end

function GamemodeFrameworkManager:LoadGamemodeHooks(gamemode)
    if gamemode._config.Hooks then
        HooksModule:new(gamemode, gamemode._config.Hooks)
    end
end
