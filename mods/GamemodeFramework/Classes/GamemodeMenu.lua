GamemodeMenu = GamemodeMenu or class()

GamemodeMenu._menu_name = "GamemodeMenuNode"

function GamemodeMenu:init()
    self:RegisterHooks()
end

function GamemodeMenu:InitializeNode(parent_node)
    for i, gamemode in pairs(GamemodeFramework.managers.framework._loaded_configs) do
        local localize_title = managers.localization:exists(gamemode.data.id .. "TitleID")
        local localize_desc = managers.localization:exists(gamemode.data.id .. "DescID")

        MenuHelperPlus:AddToggle({
            id = gamemode.data.id .. "Button",
            title = localize_title and gamemode.data.id .. "TitleID" or gamemode.data.name,
            node = parent_node,
            desc = localize_desc and gamemode.data.id .. "DescID" or gamemode.data.description,
            callback = "GamemodeSelectedChanged",
            value = false,
            localized = localize_title,
            localized_help = localize_desc,
            merge_data = {
                gamemode_key = i
            }
        })
    end
end

function GamemodeMenu:RegisterHooks()
    Hooks:Add("MenuManagerSetupCustomMenus", "BuildGamemodeMenu", function(self_menu)
        local main_node = MenuHelperPlus:NewNode(nil, {
            name = self._menu_name,

            --Was trying to get the same faded background effect the other lobby options have

            --topic_id = "menu_selecting_gamemode",
            --align_line = 0.75,
            --stencil_align="manual",
            --stencil_image="guis/textures/bg_lobby_fullteam",

            merge_data = {

                --use_info_rect=false,
                --stencil_align_percent=65,

                sync_state="gamemode_options"
            }
        })

        MenuCallbackHandler.GamemodeSelectedChanged = function(this, item)
            local ticked = item:value() == "on"
            if ticked then
                for _, sub_item in pairs(main_node:items()) do
                    if sub_item.TYPE == "toggle" and sub_item ~= item then
                        -- This, aswell as the block below is for disabling the other gamemodes once one has been selected
                        --sub_item:set_enabled(false)

                        sub_item:set_value("off")
                    end
                end
            --[[else
                for _, sub_item in pairs(main_node:items()) do
                    if sub_item.TYPE == "toggle" and sub_item ~= item then
                        sub_item:set_enabled(true)
                    end
                end]]--
            end

            GamemodeFramework.managers.framework:set_selected_gamemode(item._parameters.gamemode_key)
        end

        self:InitializeNode(main_node)

        managers.menu:add_back_button(main_node)

        MenuHelperPlus:AddButton({
            id = self._menu_name .. "Button",
            title = self._menu_name .. "ButtonTitleID",
            node_name = "lobby",
            next_node = self._menu_name,
            position = 3,
            merge_data = {
                visible_callback = "is_server"
            }
        })
    end)
end
