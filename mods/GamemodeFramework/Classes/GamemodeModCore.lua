GamemodeModCore = GamemodeModCore or class(ModCore)
GamemodeModCore._ignored_modules = {"Hooks"}

function GamemodeModCore:init(config_path, load_modules)
    if not io.file_is_readable(config_path) then
        self:log("[ERROR] Config file is not readable!")
        return
    end
    self.super.init(self, config_path, load_modules)
end
